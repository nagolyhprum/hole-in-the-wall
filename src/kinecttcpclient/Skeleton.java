package kinecttcpclient;

import java.util.ArrayList;
import java.util.Iterator;

public class Skeleton {

    public static final int HIP_CENTER = 0,
            SPINE = 1,
            SHOULDER_CENTER = 2,
            HEAD = 3,
            SHOULDER_LEFT = 4,
            ELBOW_LEFT = 5,
            WRIST_LEFT = 6,
            HAND_LEFT = 7,
            SHOULDER_RIGHT = 8,
            ELBOW_RIGHT = 9,
            WRIST_RIGHT = 10,
            HAND_RIGHT = 11,
            HIP_LEFT = 12,
            KNEE_LEFT = 13,
            ANKLE_LEFT = 14,
            FOOT_LEFT = 15,
            HIP_RIGHT = 16,
            KNEE_RIGHT = 17,
            ANKLE_RIGHT = 18,
            FOOT_RIGHT = 19,
            SIZE = 120,
            JOINTS = 20;
    
    public static final float LEG_SCALE = 1.2f;
    public static final float TORSO_SCALE = 1.4f;
    public static final float FOREARM_SCALE = 0.8f;

    public static class Combination {

        public int a, b;
        public float scale;

        public Combination(int a, int b) {
            this.a = a;
            this.b = b;
            scale = 1.0f;
        }

        public Combination(int a, int b, float scale) {
            this.a = a;
            this.b = b;
            this.scale = scale;
        }
    }
    public static final Combination[] COMBINATIONS = new Combination[]{
        //right leg
        new Combination(FOOT_RIGHT, ANKLE_RIGHT),
        new Combination(ANKLE_RIGHT, KNEE_RIGHT, LEG_SCALE),
        new Combination(KNEE_RIGHT, HIP_RIGHT, TORSO_SCALE),
        //left leg
        new Combination(FOOT_LEFT, ANKLE_LEFT),
        new Combination(ANKLE_LEFT, KNEE_LEFT, LEG_SCALE),
        new Combination(KNEE_LEFT, HIP_LEFT, TORSO_SCALE),
        //right arm
        new Combination(HAND_RIGHT, WRIST_RIGHT, FOREARM_SCALE),
        new Combination(WRIST_RIGHT, ELBOW_RIGHT, FOREARM_SCALE),
        new Combination(ELBOW_RIGHT, SHOULDER_RIGHT),
        //left arm
        new Combination(HAND_LEFT, WRIST_LEFT, FOREARM_SCALE),
        new Combination(WRIST_LEFT, ELBOW_LEFT, FOREARM_SCALE),
        new Combination(ELBOW_LEFT, SHOULDER_LEFT),
        //upperbody
        new Combination(SHOULDER_CENTER, SHOULDER_LEFT),
        new Combination(SHOULDER_CENTER, HEAD),
        new Combination(SHOULDER_CENTER, SHOULDER_RIGHT),
        new Combination(SPINE, SHOULDER_CENTER, TORSO_SCALE),
        new Combination(SHOULDER_LEFT, SPINE),
        new Combination(SHOULDER_RIGHT, SPINE),            
        //lowerbody
        new Combination(HIP_CENTER, HIP_RIGHT, TORSO_SCALE),
        new Combination(HIP_CENTER, HIP_LEFT, TORSO_SCALE),
        new Combination(HIP_CENTER, SPINE, TORSO_SCALE)
    };
    private float[][] joints3D;

    private Skeleton() {
    }

    public static Skeleton makeSkeleton(int[][] data) {
        Skeleton skeleton = new Skeleton();
        skeleton.joints3D = new float[JOINTS][3];
        for (int i = 0; i < JOINTS; i++) {
            skeleton.joints3D[i][0] = data[i][1] / 1000f;
            skeleton.joints3D[i][1] = data[i][2] / 1000f;
            skeleton.joints3D[i][2] = data[i][3] / 1000f;
        }
        return skeleton;
    }

    public float[] get3DJoint(int index) {
        return joints3D[index];
    }
}
