

package kinecttcpclient;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.*;

/**Class providing output panels for KinectTCPClient image and depth data. Contains
 * nested classes for different image purposes.
 *
 * @author Rolf Lakaemper
 */
public class KinectOutput extends JFrame{

    // ========================================================================
    // Class: Image Panel
    // ========================================================================
    /** ImagePanel: For depth and rgb image
     * 
     */
    public class ImagePanel extends JPanel{
        int sizeX, sizeY;
        BufferedImage image = null;
        Color color = Color.black;
        String name;

        // --------------------------------------------------------------------
        public ImagePanel(int sx, int sy){
            super();
            sizeX = sx;
            sizeY = sy;
            image = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_3BYTE_BGR);

            setPreferredSize(new Dimension(sx,sy));
        }
        
        // --------------------------------------------------------------------
        public void paintComponent(Graphics g){
            Graphics2D g2 = (Graphics2D)g;

            if (image != null){
                //AffineTransform at = new AffineTransform();
                
                double scale = (double)sizeX / image.getWidth();


                AffineTransform tx = AffineTransform.getScaleInstance(-scale, scale);
                tx.translate(-image.getWidth(null), 0);




                AffineTransformOp scaleOp = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
                g2.drawImage(image,scaleOp,0,0);
                g.setColor(Color.black);
                g.fillRect(0,0,45,15);
                g.setColor(Color.green);
                g.drawString("Image",5,10);
            }
            else{
                g.setColor(color);
                g.fillRect(0,0,sizeX,sizeY);
                g.setColor(Color.red);
                g.drawString("No Image",sizeX/2, sizeY/2);
            }
        }   
    }
    // ========================================================================
    // Class: DepthPanel, central projection depth view
    // ========================================================================
    /**Depth Panel, central projection depth view of point cloud.
     * Rotation and zoom by mouse.
     * 
     */
    public class DepthPanel extends JPanel implements MouseMotionListener, MouseListener, Comparator{
        public boolean painting = false;
        private boolean mouseEntered = false;
        private boolean mousePressed = false;
        private int mousebutton = 0;
        private int mouseLastX, mouseLastY;
        int sizeX, sizeY;
        double [][]projection = null;
        int [][]xyz = null;
        int maxX, maxY, minX, minY;
        int []rgb = null;
        double scale;
        double PLANEDISTANCE = 300.0;                                           // distance to projection plane
        double OPENING = PLANEDISTANCE*Math.tan(28.5/180*Math.PI);              // factor to get a 57 degree lens
        double RCX = 0.0;                                                       // rotation center
        double RCY = 0.0;
        double RCZ = 1800.0;
        double rotY = 45.0*Math.PI/180.0;                                       // Yaw
        double rotX = 0.0*Math.PI/180.0;                                        // Pitch

        double TRANSLATEDISTANCE = 0;
        double [][]rotMatrix;

        public DepthPanel(int sx, int sy){
            super();
            sizeX = sx;
            sizeY = sy;
            setPreferredSize(new Dimension(sx,sy));
            rotMatrix = new double[3][3];
            determineRotMatrix();
            this.addMouseListener(this);
            this.addMouseMotionListener(this);
        }

        // --------------------------------------------------------------------
        public void setViewParameters(double irX, double irY, double iDist){
            rotX = irX/180*Math.PI;
            rotY = irY/180*Math.PI;
            TRANSLATEDISTANCE = iDist;
            this.determineRotMatrix();
        }
        // --------------------------------------------------------------------
        private void determineRotMatrix(){
            double [][]ry = new double[3][3];
            double [][]rx = new double[3][3];

            double cs = Math.cos(rotX);
            double sn = Math.sin(rotX);
            // rot around X
            rx[0][0] = 1.0;
            rx[1][1] = cs;  rx[1][2] = -sn;
            rx[2][1] = sn; rx[2][2] = cs;

            // rot around Y
            cs = Math.cos(rotY);
            sn = Math.sin(rotY);
            ry[1][1] = 1.0;
            ry[0][0] = cs;  ry[0][2] = -sn;
            ry[2][0] = sn; ry[2][2] = cs;

            // matrix multiplication
            for (int i=0; i<3; i++){
                for (int j=0; j<3; j++){
                    rotMatrix[i][j] = 0.0;
                    for (int k = 0; k<3; k++){
                        rotMatrix[i][j] += rx[i][k]*ry[k][j];
                    }
                }
            }
        }

        // --------------------------------------------------------------------
        private int[]translate(int[]vec, int factor){
            vec[0] -= RCX*factor;
            vec[1] -= RCY*factor;
            vec[2] -= RCZ*factor;
            return(vec);
        }
        // --------------------------------------------------------------------
        private int[]rotate(int[]vec){

            if (rotMatrix != null){
                double []result = new double[3];
                for (int i=0; i<3; i++){
                    for (int j = 0; j<3; j++){
                        result[i] += rotMatrix[i][j]*vec[j];
                    }
                }
                vec[0] = (int)(result[0]);
                vec[1] = (int)(result[1]);
                vec[2] = (int)(result[2]);
            }
            return(vec);
        }
        
        // --------------------------------------------------------------------
        @Override
        public void paintComponent(Graphics g){
            painting = true;    // semaphor (this is bad...)

            // short flash when mouse enters
            if (!mouseEntered){
                g.setColor(Color.black);
            }else{
                g.setColor(new Color(30,100,0));
                mouseEntered = false;
            }
            g.fillRect(0,0,sizeX,sizeY);
            g.setColor(Color.white);

            //
            if (xyz == null){
                g.setColor(Color.red);
                g.drawString("No Point Cloud", sizeX/2, sizeY/2);
            }
            else{
                // transform
                // (a) rotate around RCX, RCY, RCZ
                for (int i = 0; i<xyz.length; i++){
                    xyz[i] = translate(xyz[i],1);
                    xyz[i] = rotate(xyz[i]);
                    xyz[i] = translate(xyz[i],-1);
                }

                // add TRANSLATEDISTANCE to z

                for (int i = 0; i<xyz.length; i++){
                    xyz[i][2] += TRANSLATEDISTANCE;
                }

                // sorting
                //Arrays.sort(xyz,this);

                // project
                projection = new double[xyz.length][3];
                int scrRadX = sizeX/2;
                int scrRadY = sizeY/2;
                for (int index = 0; index<xyz.length; index++){
                    if (xyz[index][2] > PLANEDISTANCE){
                        // projection
                        double x = (int)Math.round(PLANEDISTANCE*(double)xyz[index][0]/(double)xyz[index][2]);
                        double y = (int)Math.round(PLANEDISTANCE*(double)xyz[index][1]/(double)xyz[index][2]);
                        // opening angle 53 degrees
                        x = x*scrRadX/OPENING + scrRadX;
                        y = y*scrRadX/OPENING + scrRadY;
                        projection[index][0] = x;
                        projection[index][1] = y;
                        
                        // distance => grey (for fog)
                        projection[index][2]=(int)(Math.sqrt(xyz[index][0]*xyz[index][0]+xyz[index][1]*xyz[index][1]+xyz[index][2]*xyz[index][2]));
                        projection[index][2] = (8000-projection[index][2])*256/(8000-PLANEDISTANCE);
                        projection[index][2] = Math.min(255,projection[index][2]);
                        projection[index][2] = Math.max(20,projection[index][2]);

                    }
                }


                // plot
                for(int index = 0; index<xyz.length; index++){
                    int c = (int)projection[index][2];
                    if (xyz[0].length==4 && xyz[index][3]>0){
                        g.setColor(new Color(c,0,0));
                    } else {
                        g.setColor(new Color(c,c,c));
                    }
                    int ix = (int)Math.round(projection[index][0]);
                    int iy = (int)Math.round(projection[index][1]);
                    g.drawLine(ix,iy,ix,iy);
                }
            }
            painting = false;
            g.setColor(Color.black);
            g.fillRect(0,0,75,15);
            g.setColor(Color.green);
            g.drawString("Point Cloud",5,10);
        }

        public void mouseDragged(MouseEvent e) {
            double dx = e.getX() - mouseLastX;
            double dy = e.getY() - mouseLastY;
            mouseLastX = e.getX();
            mouseLastY = e.getY();

            if (mousebutton == 1){
                rotX += dy/100;
                rotY += dx/100;
                this.determineRotMatrix();
            }else{
                TRANSLATEDISTANCE +=dy*10;
            }
            
        }

        public void mouseMoved(MouseEvent e) {

        }

        public void mouseClicked(MouseEvent e) {

        }

        public void mousePressed(MouseEvent e) {
            this.mousePressed = true;
            mouseLastX = e.getX();
            mouseLastY = e.getY();
            mousebutton = e.getButton();
        }

        public void mouseReleased(MouseEvent e) {
            mousebutton = 0;
        }

        public void mouseEntered(MouseEvent e) {
            mouseEntered = true;
        }

        public void mouseExited(MouseEvent e) {

        }

        public int compare(Object o1, Object o2) {
            if (((int [])o1)[2] >  ((int [])o2)[2])
                return(-1);
            else
                return(1);
        }
    }

    // ========================================================================
    // Class: SkeletonPanel
    // ========================================================================
    /** Skeleton Panel, shows skeleton data.
     * 
     */
    public class SkeletonPanel extends JPanel{
        int sizeX, sizeY;
        double scale;
        int [] data;
        int[][] joints;

        public SkeletonPanel(int sx, int sy){
            super();
            sizeX = sx;
            sizeY = sy;

            setPreferredSize(new Dimension(sx,sy));
        }

        // --------------------------------------------------------------------
        public void paintComponent(Graphics g){
            g.setColor(Color.black);
            g.fillRect(0,0,sizeX,sizeY);
            g.setColor(Color.white);

            if (data != null && data[0]>0){
                try{
                    
                    for (int skelIndex = 1; skelIndex<=2; skelIndex++){
                        joints = KinectTCPClient.getJointPositions(data, skelIndex);
                        int[][]segments = new int[19][5];
                        // head to shoulder center
                        segments[0][0] = joints[3][1];
                        segments[0][1] = joints[3][2];
                        segments[0][2] = joints[2][1];
                        segments[0][3] = joints[2][2];
                        segments[0][4] = joints[3][0]*joints[2][0];

                        //shoulder center to shoulder right
                        segments[1][0] = joints[8][1];
                        segments[1][1] = joints[8][2];
                        segments[1][2] = joints[2][1];
                        segments[1][3] = joints[2][2];
                        segments[1][4] = joints[8][0]*joints[2][0];

                        //shoulder right to elbow right
                        segments[2][0] = joints[8][1];
                        segments[2][1] = joints[8][2];
                        segments[2][2] = joints[9][1];
                        segments[2][3] = joints[9][2];
                        segments[2][4] = joints[8][0]*joints[9][0];

                        //elbow right to wrist right
                        segments[3][0] = joints[10][1];
                        segments[3][1] = joints[10][2];
                        segments[3][2] = joints[9][1];
                        segments[3][3] = joints[9][2];
                        segments[3][4] = joints[10][0]*joints[9][0];

                        //wrist right to hand right
                        segments[4][0] = joints[10][1];
                        segments[4][1] = joints[10][2];
                        segments[4][2] = joints[11][1];
                        segments[4][3] = joints[11][2];
                        segments[4][4] = joints[10][0]*joints[11][0];

                        //
                        //shoulder center to shoulder left
                        segments[5][0] = joints[4][1];
                        segments[5][1] = joints[4][2];
                        segments[5][2] = joints[2][1];
                        segments[5][3] = joints[2][2];
                        segments[5][4] = joints[2][0]*joints[4][0];

                        //shoulder left to elbow left
                        segments[6][0] = joints[4][1];
                        segments[6][1] = joints[4][2];
                        segments[6][2] = joints[5][1];
                        segments[6][3] = joints[5][2];
                        segments[6][4] = joints[4][0]*joints[5][0];

                        //elbow left to wrist left
                        segments[7][0] = joints[5][1];
                        segments[7][1] = joints[5][2];
                        segments[7][2] = joints[6][1];
                        segments[7][3] = joints[6][2];
                        segments[7][4] = joints[6][0]*joints[5][0];

                        //wrist left to hand left
                        segments[8][0] = joints[6][1];
                        segments[8][1] = joints[6][2];
                        segments[8][2] = joints[7][1];
                        segments[8][3] = joints[7][2];
                        segments[8][4] = joints[7][0]*joints[6][0];

                        // spine
                        segments[9][0] = joints[2][1];
                        segments[9][1] = joints[2][2];
                        segments[9][2] = joints[1][1];
                        segments[9][3] = joints[1][2];
                        segments[9][4] = joints[1][0]*joints[2][0];

                        segments[10][0] = joints[1][1];
                        segments[10][1] = joints[1][2];
                        segments[10][2] = joints[0][1];
                        segments[10][3] = joints[0][2];
                        segments[10][4] = joints[0][0]*joints[1][0];

                        //right leg
                        segments[11][0] = joints[16][1];
                        segments[11][1] = joints[16][2];
                        segments[11][2] = joints[0][1];
                        segments[11][3] = joints[0][2];
                        segments[11][4] = joints[0][0]*joints[16][0];

                        segments[12][0] = joints[16][1];
                        segments[12][1] = joints[16][2];
                        segments[12][2] = joints[17][1];
                        segments[12][3] = joints[17][2];
                        segments[12][4] = joints[17][0]*joints[16][0];

                        segments[13][0] = joints[17][1];
                        segments[13][1] = joints[17][2];
                        segments[13][2] = joints[18][1];
                        segments[13][3] = joints[18][2];
                        segments[13][4] = joints[18][0]*joints[17][0];

                        segments[14][0] = joints[18][1];
                        segments[14][1] = joints[18][2];
                        segments[14][2] = joints[19][1];
                        segments[14][3] = joints[19][2];
                        segments[14][4] = joints[19][0]*joints[18][0];

                        //left leg
                        segments[15][0] = joints[12][1];
                        segments[15][1] = joints[12][2];
                        segments[15][2] = joints[0][1];
                        segments[15][3] = joints[0][2];
                        segments[15][4] = joints[0][0]*joints[12][0];

                        segments[16][0] = joints[12][1];
                        segments[16][1] = joints[12][2];
                        segments[16][2] = joints[13][1];
                        segments[16][3] = joints[13][2];
                        segments[16][4] = joints[13][0]*joints[12][0];

                        segments[17][0] = joints[13][1];
                        segments[17][1] = joints[13][2];
                        segments[17][2] = joints[14][1];
                        segments[17][3] = joints[14][2];
                        segments[17][4] = joints[14][0]*joints[13][0];

                        segments[18][0] = joints[14][1];
                        segments[18][1] = joints[14][2];
                        segments[18][2] = joints[15][1];
                        segments[18][3] = joints[15][2];
                        segments[18][4] = joints[15][0]*joints[14][0];


                        int z = joints[1][3];
                        double zscale = z/500.0;

                        if (skelIndex == 1)
                            g.setColor(Color.red);
                        else
                            g.setColor(Color.blue);

                        int headsize = 50;
                        //g.fillOval((segments[0][0]-headsize/2)/4+300, -(segments[0][1]-headsize/2)/4+180,headsize,headsize);

                        for (int i = 0; i<segments.length; i++){
                            if (segments[i][4]!=0){
                                int x1 = (int)(segments[i][0]/zscale+320.0);
                                int y1 = (int)(-segments[i][1]/zscale+200.0);
                                int x2 = (int)(segments[i][2]/zscale+320.0);
                                int y2 = (int)(-segments[i][3]/zscale+200.0);
                                g.drawLine(x1, y1, x2, y2);
                            }
                        }
                    }
                }catch(Exception e){
                    //System.out.println(e.getMessage());
                }
            }
            g.setColor(Color.black);
            g.fillRect(0,0,55,15);
            g.setColor(Color.green);
            g.drawString("Skeleton",5,10);
        }
    }

    // ========================================================================
    // Main Class: KinectOutput
    // ========================================================================
    /** Main class: kinect output. Frames panels for depth, rgb, point cloud and skeleton.
     * 
     */
    public ImagePanel depth, rgb;
    public DepthPanel depthXYZ;
    public SkeletonPanel skeleton;

    public KinectOutput(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        depth = new ImagePanel(640,480);
        depth.color = Color.blue;
        rgb = new ImagePanel(640,480);
        rgb.color = Color.red;
        depthXYZ = new DepthPanel(640,480);
        skeleton = new SkeletonPanel(640,480);
        this.getContentPane().setLayout(new GridLayout(2,2));
        this.getContentPane().add(depth);
        this.getContentPane().add(rgb);
        this.getContentPane().add(depthXYZ);
        this.getContentPane().add(skeleton);
        pack();
        this.setVisible(true);
    }

    // --------------------------------------------------------------------
    public void setImage(ImagePanel panel, int []pixels){
        int base = (int) (Math.sqrt(pixels.length/12));
        int resX = base*4;
        int resY = base*3;
        panel.image = new BufferedImage(resX, resY, BufferedImage.TYPE_3BYTE_BGR);
        panel.image.setRGB(0,0,resX,resY,pixels,0,resX);
        panel.repaint();
    }

    public static int []convertDepthToImage(int [][]depth){
        // convert to color
        int []pixels = new int[depth.length];
        // color table for index (detected humans)
        int [][]col = {{200,0,0},{0,200,0},{0,0,200},{200,200,0},{0,200,200},{200,0,200}};

        for (int i=0; i<pixels.length; i++){
            int distance = depth[i][0];
            int red,green,blue;
            int index = depth[i][1];
            if (index>0){
                red = Math.min(col[index-1][0]+(int)(Math.random()*255),255);
                green = Math.min(col[index-1][1]+(int)(Math.random()*255),255);
                blue = Math.min(col[index-1][2]+(int)(Math.random()*255),255);

//                red = (int)(Math.random()*255);
//                green = (int)(Math.random()*255);
//                blue = (int)(Math.random()*255);

            }
            else{
                blue = 255*(distance - 300) / (1500 - 300);
                green = 255*(distance - 1500) / (2500 - 1500);
                red = 255*(distance - 2500) / (6000 - 2500);
                if (blue < 0) blue = 0;
                if (blue > 512) blue = 0;
                if (blue > 255) blue = 512-blue;
                if (green < 0) green = 0;
                if (green > 512) green = 0;
                if (green > 255) green = 512-green;
                if (red < 0) red = 0;
                if (red > 512) red = 0;
                if (red > 255) red = 512 - red;
            }
            pixels[i] = (0xff<<24 | red<<16 | green << 8 | blue);
        }
        return(pixels);
    }


    // -----------------------------------------------------------------------
    public void displaySkeleton(int []skeletonStream){
        skeleton.data = skeletonStream;
        skeleton.repaint();
    }

    // -----------------------------------------------------------------------
    public void displayDepthXYZ(int[][] xyz){
        while(depthXYZ.painting){
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
            }
        }
        depthXYZ.xyz = xyz;
        depthXYZ.repaint();
    }
}
