package kinecttcpclient;

/**
 * An example application that connects to a kinectTCP server and shows all
 * data: depth, rgb, point cloud, skeleton, audio. Double clicking the
 * executable jar file executes this file.
 *
 * @author Rolf Lakaemper
 */
public class Main {    

    // ------------------------------------------------------------------------
    public static void main(String[] args) throws InterruptedException {
        String ipaddress = "129.32.94.70";
        int port = 8001;
//        KinectTCPClient clAudio = new KinectTCPClient(ipaddress,port);
//        clAudio.start();
//        Thread.sleep(200);

        //VirtualJoystick vj = new VirtualJoystick(ipaddress,port);

        KinectTCPClient cl = new KinectTCPClient(ipaddress, port);
        KinectOutput ko = new KinectOutput();
        ko.depthXYZ.setViewParameters(25, 45, 2000);

        // being polite
        cl.sayHello();
        int a = cl.startKinect(true);

        //byte[]data = {1,2,3,4,-1};
        //cl.writeDataToCommunactionChannel(0, data);
        //int [] dataInt = cl.readDataFromCommunactionChannel(0);


        // start loop
        long time = System.currentTimeMillis();
        long frames = 0;
        while (true) {
            frames++;

            // Depth - - - - -          
            int[] depthImage;
            if (true) {
                int[][] depthValues;
                depthValues = cl.readDepth();
                depthImage = KinectOutput.convertDepthToImage(depthValues);
                ko.setImage(ko.depth, depthImage);
            }

            // RGB - - - - -
            int[] rgbPixels;
            if (true) {
                rgbPixels = cl.readRGBImage();
                ko.setImage(ko.rgb, rgbPixels);
            }

            // DepthXYZ - - - - -
            if (true) {
                int[][] depthXYZ = null;
                depthXYZ = cl.readDepthXYZIndex();
                ko.displayDepthXYZ(depthXYZ);
            }

            // Skeleton - - - - -
            if (true) {
                int[] skeletonStream = cl.readSkeleton();
                ko.displaySkeleton(skeletonStream);
            }

            //ImageCalibrator imcal = new ImageCalibrator(rgbPixels,depthImage);
            //System.out.println("best: "+imcal.overlapBest);

            //----------------------------------------------
            //stats
            Thread.sleep(100);
            long t = System.currentTimeMillis() - time;
            double fps = 1000.0 * frames / (double) t;
            if (frames % 100 == 0) {
                //System.out.println(fps+" frames per second (last timestamp:"+(long)cl.timestamp+")");
            }
        }

    }
}
