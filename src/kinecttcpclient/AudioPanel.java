/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kinecttcpclient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

/**Panel needed for example audio stream output of KinectTCPClient.
 *
 * @author Rolf Lakaemper
 */
public class AudioPanel extends JPanel{
    int buffersize = 512;

    int[] buffer = new int[buffersize/4];
    public AudioPanel(){
        setPreferredSize(new Dimension(1024,480));
    }
    public void paintComponent(Graphics g){
        g.setColor(Color.black);
        g.fillRect(0,0,1024,480);
        g.setColor(Color.green);
        int scale = 1024/buffersize*4;
        int yOld;
        yOld = buffer[0]/50+240;
        for (int i = 1; i<buffer.length; i++){
            int y = buffer[i]/50+240;
            g.drawLine((i-1)*scale,yOld,i*scale,y);
            yOld = y;
        }
    }
}
