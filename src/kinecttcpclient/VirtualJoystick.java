package kinecttcpclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**A virtual joystick that sends messages to kinectTCP's channel 0. Can for example
 * be used to remotely control robots etc. The messages sent are one byte messages, reflecting
 * a pre-specified area which the mouse pointer is over.
 * 
 * @author mrsofty
 */
public class VirtualJoystick extends JPanel implements MouseMotionListener, MouseListener{
    int L = 50;
    int sizeX = 5*L;
    int sizeY = 5*L;
    int [][]regions;
    Color[] acolors;
    Color[] bcolors;
    JTextField text;
    KinectTCPClient cl;

    boolean joyActive = false;
    volatile int joyArea;
    
    public VirtualJoystick(String ipaddress, int port){
        
        cl = new KinectTCPClient(ipaddress,port);

        setPreferredSize(new Dimension(sizeX,sizeY));
        defineRegions();
        this.addMouseMotionListener(this);
        this.addMouseListener(this);

        text = new JTextField("  Not Connected");
        text.setBackground(new Color(0,0,0));
        text.setForeground(new Color(100,0,100));

        if (cl != null){
            text.setText("Connected to KinectTCP on "+ipaddress);
        }

        JFrame f = new JFrame("VirtualJoystick");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().setLayout(new BorderLayout());
        f.getContentPane().add(this,"Center");
        f.getContentPane().add(text,"South");
        f.pack();
        f.setVisible(true);

    }
    
    private void defineRegions(){
        int [][] r = {{2,2,1,1,0},{2,1,1,1,1},{2,0,1,1,2},{2,3,1,1,1},{2,4,1,1,2},
            {1,2,1,1,1},{0,2,1,1,2},{3,2,1,1,1},{4,2,1,1,2},
            {0,0,2,2,3},{3,0,2,2,3},{0,3,2,2,3},{3,3,2,2,3}};

        regions = new int[r.length][5];
        for (int i=0; i<r.length; i++){
            for (int j=0; j<4; j++){
                regions[i][j] = r[i][j] * L;
            }
            regions[i][4] = r[i][4];
        }
        acolors = new Color[4];
        acolors[0] = new Color(50,0,0);
        acolors[1] = new Color(25,60,0);
        acolors[2] = new Color(10,100,20);
        acolors[3] = new Color(50,50,5);

        bcolors = new Color[4];
        bcolors[0] = new Color(20,0,0);
        bcolors[1] = new Color(12,30,0);
        bcolors[2] = new Color(6,50,10);
        bcolors[3] = new Color(26,26,3);
    }

    int checkBox(int x, int y){
        int i;
        boolean found = false;
        for (i=0; i<regions.length; i++){
            if (x>=regions[i][0] && x<=regions[i][0]+regions[i][2] && y>=regions[i][1] && y<=regions[i][1]+regions[i][3]){
                found = true;
                break;
            }
        }
        if (!found){
            i = -1;
        }
        return(i);
    }

    public void sendMessage(int message){
        byte [] data = new byte[1];
        data[0] = (byte)message;
        cl.writeDataToCommunactionChannel(0, data);
        System.out.println(System.currentTimeMillis() + " " + message);
    }

    public void paintComponent(Graphics g){
        g.setColor(Color.black);
        g.fillRect(0,0,sizeX,sizeY);
        for (int i = 0; i<regions.length; i++){
            if (joyActive)
                g.setColor(acolors[regions[i][4]]);
            else
                g.setColor(bcolors[regions[i][4]]);
            g.fillRect(regions[i][0],regions[i][1],regions[i][2],regions[i][3]);
            if (joyActive)
                g.setColor(new Color(100,200,10));
            else
                g.setColor(new Color(100,0,100));
            g.drawRect(regions[i][0],regions[i][1],regions[i][2]-1,regions[i][3]-1);
        }

        if (joyActive){
            g.setColor(new Color(100,0,100));
            g.fillRect(regions[joyArea][0],regions[joyArea][1],regions[joyArea][2],regions[joyArea][3]);
            g.setColor(new Color(200,200,100));
            g.drawRect(regions[joyArea][0],regions[joyArea][1],regions[joyArea][2]-1,regions[joyArea][3]-1);
        }
    }
    
    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        int area = checkBox(x,y);
        if (area == -1){
            if (joyActive){
                sendMessage(0);
            }
            joyActive = false;
        } else {
            if (joyActive && joyArea != area){
                sendMessage(area+1);
            }
            joyArea = area;
        }
        repaint();
    }

    public void mouseClicked(MouseEvent e) {
        joyActive = !joyActive;
        if (!joyActive){
            sendMessage(0);
        } else {
            joyArea = -1;
            this.mouseMoved(e);
        }
        repaint();
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {
    }

}
