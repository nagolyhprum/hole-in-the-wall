package holeinthewall;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.SpotLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.debug.Arrow;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Cylinder;
import com.jme3.scene.shape.Line;
import com.jme3.system.AppSettings;
import java.awt.event.WindowEvent;
import java.util.Iterator;
import kinecttcpclient.KinectTCPClient;
import kinecttcpclient.Skeleton;
import kinecttcpclient.Skeleton.Combination;

public class PhysicalBody extends SimpleApplication implements PhysicsCollisionListener {

    public static final int ROWS = 31;
    public static final int COLUMNS = 35;
    public static final int MAX_PLAYERS = 2;
    public static final float COL_SPAN = 0.04f;
    public static final float ROW_SPAN = 0.04f;
    public static final float THICKNESS = 0.04f;
    public static final float GAP = 0.05f;
    public static final float WIDTH = COLUMNS * COL_SPAN + (COLUMNS - 1) * GAP;
    public static final float HEIGHT = ROWS * ROW_SPAN + (ROWS - 1) * GAP;
    public static final float PLAYER_SCALE = 1.3f;
    public static final float PLAYER_X_DISPLACEMENT = 0.2f;
    public static final float PLAYER_Y_DISPLACEMENT = 1.1f;
    public static final float FLOOR_LENGTH = 5f;
    public static final float WALL_START = -3f;
    public static final ColorRGBA[] PLAYER_COLORS = {ColorRGBA.Red, ColorRGBA.Blue};
    private static boolean debugMode = false;
    private static String kinectAddress;
    private boolean resetRequested;
    private float wallSpeed = 1.0f;
    private float lightIntensity = 0.5f;

    public enum GameState {

        ROUND_START, SOLID_MOVING, HOLE_WALL, HOLE_MOVING
    };
    private Material matRed, matGreen, matBlue;
    private Material brokeWallMat, normWallMat, playerMat, floorMat;
    private Geometry[] bones;
    private Geometry[] bricks;
    private RigidBodyControl[] bricksPhys;
    private boolean[] brickScored;
    private Node wall;
    private Node skeleton;
    private KinectTCPClient tcp;
    private BulletAppState bas;
    private SpotLight camSpot;
    private GameState curState;
    private int curPlayer;
    private int[] playerScores = new int[MAX_PLAYERS];
    private int[] playerTurnsTaken = new int[MAX_PLAYERS];
    private BitmapText topLeftScore, topRightScore;
    private ActionListener roundTrigger = new ActionListener() {
        public void onAction(String name, boolean isPressed, float tpf) {
            if ((!isPressed) && name.equals("start round")) {
                if (curState == GameState.ROUND_START || curState == GameState.HOLE_WALL) {
                    curState = GameState.values()[(curState.ordinal() + 1) % GameState.values().length];
                    System.out.println("game state = " + curState.toString());
                }
            }
        }
    };
    private DemoControlJFrame demoControl;
    AudioNode applause, suspense, calm, collision;

    public static void main(String[] args) {
        if (args.length > 0) {
            kinectAddress = args[0];
        } else {
            kinectAddress = "127.0.0.1";
        }
        PhysicalBody app = new PhysicalBody();
        AppSettings settings = new AppSettings(true);
        settings.setResolution(1024, 768);
        //settings.setBitsPerPixel(32);
        settings.setFrameRate(60);
        settings.setVSync(true);
        app.setSettings(settings);
        app.setShowSettings(true);
        app.setPauseOnLostFocus(false);
        app.setDisplayFps(false);
        app.setDisplayStatView(false);
        app.start();
    }

    @Override
    public void simpleInitApp() {
        bas = new BulletAppState();
        stateManager.attach(bas);
        bas.getPhysicsSpace().addCollisionListener(this);
        tcp = new KinectTCPClient(kinectAddress, 8001);
        flyCam.setMoveSpeed(15);
        flyCam.setDragToRotate(true);
        initMaterials();
        initCoordCross();
        initBones();
        initWall();
        initLights();
        initFloor();
        initScores();
        cam.setLocation(new Vector3f(0, HEIGHT * 1.1f, FLOOR_LENGTH * 2.0f));
        cam.lookAt(wall.getWorldTranslation(), Vector3f.UNIT_Y);
        inputManager.addMapping("start round", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addListener(roundTrigger, "start round");
        curState = GameState.ROUND_START;
        curPlayer = 0;

        applause = new AudioNode(assetManager, "Sounds/applause.wav", false);
        rootNode.attachChild(applause);
        suspense = new AudioNode(assetManager, "Sounds/suspense.wav", false);
        suspense.setLooping(true);
        rootNode.attachChild(suspense);
        calm = new AudioNode(assetManager, "Sounds/calm.wav", false);
        calm.setLooping(true);
        rootNode.attachChild(calm);
        calm.play();
        collision = new AudioNode(assetManager, "Sounds/collision.wav", false);
        rootNode.attachChild(collision);
        //debug physics
        if (debugMode) {
            bas.getPhysicsSpace().enableDebug(assetManager);
        }

        demoControl = new DemoControlJFrame(this);
        demoControl.setVisible(true);
        resetRequested = false;
    }

    /**
     * Make a solid floor and add it to the scene.
     */
    public void initFloor() {
        /**
         * Initialize the floor geometry
         */
        Box floor = new Box(WIDTH / 2.0f, 0.05f, FLOOR_LENGTH);
        floor.scaleTextureCoordinates(new Vector2f(3, 6));
        Geometry floor_geo = new Geometry("Floor", floor);
        floor_geo.setMaterial(floorMat);
        floor_geo.setLocalTranslation(0, -0.1f, 0);
        this.rootNode.attachChild(floor_geo);
        /* Make the floor physical with mass 0.0f! */
        RigidBodyControl floor_phy = new RigidBodyControl(0.0f);
        floor_geo.addControl(floor_phy);
        bas.getPhysicsSpace().add(floor_phy);
    }

    public void initLights() {
        /**
         * A white, directional light source
         */
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(-0.5f, -0.5f, -0.5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White.mult(0.4f));
        //rootNode.addLight(sun);
        /**
         * A white ambient light source.
         */
        AmbientLight ambient = new AmbientLight();
        ambient.setColor(ColorRGBA.DarkGray.mult(0.5f));
        rootNode.addLight(ambient);
        /**
         * A cone-shaped spotlight with location, direction, range
         */
        SpotLight spot = new SpotLight();
        spot.setSpotRange(20f);
        spot.setSpotOuterAngle(25 * FastMath.DEG_TO_RAD);
        spot.setSpotInnerAngle(15 * FastMath.DEG_TO_RAD);
        spot.setDirection(cam.getDirection());
        spot.setPosition(cam.getLocation());
        spot.setColor(ColorRGBA.White.mult(lightIntensity));
        camSpot = spot;
        rootNode.addLight(spot);
    }

    public void initWall() {
        rootNode.attachChild(wall = new Node());
        wall.setLocalTranslation(0, 0, WALL_START);
        bricks = new Geometry[COLUMNS * ROWS];
        bricksPhys = new RigidBodyControl[COLUMNS * ROWS];
        brickScored = new boolean[COLUMNS * ROWS];
        Box box = new Box(COL_SPAN, ROW_SPAN, THICKNESS);
        int row, column;
        for (int i = 0; i < bricks.length; i++) {
            Geometry brick = bricks[i] = new Geometry("brick", box);
            brick.setMaterial(normWallMat);
            brick.setLocalRotation(Matrix3f.IDENTITY);
            column = i % COLUMNS;
            row = i / COLUMNS;
            float x = column * (COL_SPAN + GAP) - WIDTH / 2.0f;
            float y = HEIGHT - row * (ROW_SPAN + GAP) + GAP;
            brick.setLocalTranslation(x, y, 0);
            RigidBodyControl rbc = new RigidBodyControl(0);
            bricks[i].addControl(rbc);
            bricksPhys[i] = rbc;
            rbc.setKinematic(true);
            bas.getPhysicsSpace().add(rbc);
            wall.attachChild(brick);
        }
    }

    public void initBones() {
        bones = new Geometry[Skeleton.COMBINATIONS.length];
        Cylinder c;
        skeleton = new Node();
        for (int i = 0; i < Skeleton.COMBINATIONS.length; i++) {
            c = new Cylinder(8, 8, 0.05f * Skeleton.COMBINATIONS[i].scale, 1, true);
            bones[i] = new Geometry("bone", c);
            bones[i].setMaterial(playerMat);
            RigidBodyControl rbc = new RigidBodyControl(50);
            bones[i].addControl(rbc);
            rbc.setKinematic(true);
            bas.getPhysicsSpace().add(rbc);
            skeleton.attachChild(bones[i]);
        }
        rootNode.attachChild(skeleton);
    }

    // -------------------------------------------------------------------------
    public void initMaterials() {
        matRed = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matRed.setColor("Color", ColorRGBA.Red);
        matGreen = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matGreen.setColor("Color", ColorRGBA.Green);
        matBlue = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        matBlue.setColor("Color", ColorRGBA.Blue);

        brokeWallMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        brokeWallMat.setBoolean("UseMaterialColors", true);
        brokeWallMat.setColor("Ambient", ColorRGBA.DarkGray);
        brokeWallMat.setColor("Diffuse", ColorRGBA.Red);
        brokeWallMat.setColor("Specular", ColorRGBA.White);
        brokeWallMat.setFloat("Shininess", 96f); // shininess from 1-128

        normWallMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        normWallMat.setBoolean("UseMaterialColors", true);
        normWallMat.setColor("Ambient", ColorRGBA.DarkGray);
        normWallMat.setColor("Diffuse", ColorRGBA.Green);
        normWallMat.setColor("Specular", ColorRGBA.White);
        normWallMat.setFloat("Shininess", 64f); // shininess from 1-128

        playerMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        playerMat.setBoolean("UseMaterialColors", true);
        playerMat.setColor("Ambient", ColorRGBA.DarkGray);
        playerMat.setColor("Diffuse", PLAYER_COLORS[0]);
        playerMat.setColor("Specular", ColorRGBA.White);
        playerMat.setFloat("Shininess", 16f); // shininess from 1-128

        floorMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        floorMat.setBoolean("UseMaterialColors", true);
        floorMat.setColor("Ambient", ColorRGBA.DarkGray);
        floorMat.setColor("Diffuse", ColorRGBA.White);
        floorMat.setColor("Specular", ColorRGBA.White);
        floorMat.setFloat("Shininess", 16f); // shininess from 1-128
    }

    // -------------------------------------------------------------------------
    private void initCoordCross() {
        Line xAxis = new Line(new Vector3f(-3, 0, 0), Vector3f.ZERO);
        Line yAxis = new Line(new Vector3f(0, -3, 0), Vector3f.ZERO);
        Line zAxis = new Line(new Vector3f(0, 0, -3), Vector3f.ZERO);
        Arrow ax = new Arrow(new Vector3f(3, 0, 0));
        Arrow ay = new Arrow(new Vector3f(0, 3, 0));
        Arrow az = new Arrow(new Vector3f(0, 0, 3));
        Geometry geomX = new Geometry("xAxis", xAxis);
        Geometry geomY = new Geometry("yAxis", yAxis);
        Geometry geomZ = new Geometry("zAxis", zAxis);
        geomX.setMaterial(matRed);
        geomY.setMaterial(matGreen);
        geomZ.setMaterial(matBlue);
        Geometry geomXA = new Geometry("xAxis", ax);
        Geometry geomYA = new Geometry("yAxis", ay);
        Geometry geomZA = new Geometry("zAxis", az);
        geomXA.setMaterial(matRed);
        geomYA.setMaterial(matGreen);
        geomZA.setMaterial(matBlue);
        rootNode.attachChild(geomX);
        rootNode.attachChild(geomY);
        rootNode.attachChild(geomZ);
        rootNode.attachChild(geomXA);
        rootNode.attachChild(geomYA);
        rootNode.attachChild(geomZA);
    }

    private void initScores() {
        topLeftScore = new BitmapText(guiFont, false);
        topLeftScore.setSize(guiFont.getCharSet().getRenderedSize() * 2);      // font size
        topLeftScore.setColor(PLAYER_COLORS[0]);                             // font color
        guiNode.attachChild(topLeftScore);
        topRightScore = new BitmapText(guiFont, false);
        topRightScore.setSize(guiFont.getCharSet().getRenderedSize() * 2);      // font size
        topRightScore.setColor(PLAYER_COLORS[1]);                             // font color
        guiNode.attachChild(topRightScore);
    }

    private void buildHoleyWall() {
        int row, column;
        for (int i = 0; i < bricks.length; i++) {
            bricks[i].setLocalRotation(Matrix3f.IDENTITY);
            column = i % COLUMNS;
            row = i / COLUMNS;
            float x = column * (COL_SPAN + GAP) - WIDTH / 2.0f;
            float y = HEIGHT - row * (ROW_SPAN + GAP) + GAP;
            bricks[i].setLocalTranslation(x, y, 0);
            if (bricks[i].getMaterial() == brokeWallMat) {
                brickScored[i] = false;
            } else {
                brickScored[i] = true;
            }
        }
        wall.setLocalTranslation(0, 0, WALL_START);
    }

    private void scoreHoleyWall() {
        float score = bricks.length - 800;
        for (int i = 0; i < bricks.length; i++) {
            if (brickScored[i] && bricks[i].getMaterial() == brokeWallMat) {
                score--;
            }
        }
        playerScores[curPlayer] += score;
    }

    public void setResetRequested() {
        resetRequested = true;
    }

    private void resetGame() {
        for (int i = 0; i < MAX_PLAYERS; i++) {
            playerScores[i] = 0;
            playerTurnsTaken[i] = 0;
        }
        resetWall();
        suspense.stop();
        calm.play();
        curPlayer = 0;
        playerMat.setColor("Diffuse", PLAYER_COLORS[curPlayer]);
        curState = GameState.ROUND_START;
        topLeftScore.setText("");
        topRightScore.setText("");
    }

    private void resetWall() {
        int row, column;
        for (int i = 0; i < bricks.length; i++) {
            bricks[i].setLocalRotation(Matrix3f.IDENTITY);
            column = i % COLUMNS;
            row = i / COLUMNS;
            float x = column * (COL_SPAN + GAP) - WIDTH / 2.0f;
            float y = HEIGHT - row * (ROW_SPAN + GAP) + GAP;
            bricks[i].setLocalTranslation(x, y, 0);
            bricks[i].setMaterial(normWallMat);
            wall.attachChild(bricks[i]);
            bricksPhys[i].setKinematic(true);
            bricksPhys[i].setMass(0);
            bricksPhys[i].setGravity(new Vector3f(0, 0, 0));
            bricksPhys[i].setEnabled(true);
        }
        wall.setLocalTranslation(0, 0, WALL_START);
    }

    private void nextPlayer() {
        curPlayer = (curPlayer + 1) % MAX_PLAYERS;
        playerMat.setColor("Diffuse", PLAYER_COLORS[curPlayer]);
    }

    private void showScores() {
        suspense.stop();
        applause.play();
        calm.play();
        int h = settings.getHeight();
        int w = settings.getWidth();
        if (playerTurnsTaken[0] == playerTurnsTaken[1]) {
            topLeftScore.setText("Player 1 score: " + playerScores[0]);
            topLeftScore.setLocalTranslation(50, h - topLeftScore.getLineHeight() * 3, 0); // position
            topRightScore.setText("Player 2 score: " + playerScores[1]);
            topRightScore.setLocalTranslation(w - topRightScore.getLineWidth() - 50, h - topRightScore.getLineHeight() * 3, 0); // position
        } else {
            topLeftScore.setText("");
            topRightScore.setText("");
        }
    }

    public void connectBones() {
        if (tcp.isAvailable()) {
            Iterator<Skeleton> skeletons = tcp.getSkeleton();
            if (skeletons.hasNext()) {
                Skeleton aSkeleton = skeletons.next();
                for (int i = 0; i < Skeleton.COMBINATIONS.length; i++) {
                    Combination c = Skeleton.COMBINATIONS[i];
                    setConnectiveTransform(aSkeleton.get3DJoint(c.a), aSkeleton.get3DJoint(c.b), bones[i], c.scale);
                }
                skeleton.setLocalTranslation(PLAYER_X_DISPLACEMENT, PLAYER_Y_DISPLACEMENT, 0);
            }
        }
    }

    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        if (resetRequested) {
            resetGame();
            resetRequested = false;
        }
        if (curState == GameState.SOLID_MOVING || curState == GameState.HOLE_MOVING) {
            wall.move(0, 0, wallSpeed * tpf);
            if (wall.getWorldTranslation().z >= FLOOR_LENGTH) {
                if (curState == GameState.SOLID_MOVING) {
                    calm.stop();
                    suspense.play();
                    buildHoleyWall();
                    nextPlayer();
                    curState = GameState.HOLE_WALL;
                } else {
                    playerTurnsTaken[curPlayer]++;
                    scoreHoleyWall();
                    resetWall();
                    curState = GameState.ROUND_START;
                    showScores();
                }
            }
        }
        connectBones();
        camSpot.setDirection(cam.getDirection());
        camSpot.setPosition(cam.getLocation());
        camSpot.setColor(ColorRGBA.White.mult(lightIntensity));
        for (int i = 0; i < bricks.length; i++) {
            if (bricks[i] != null && bricks[i].getLocalTranslation().y <= 0) {
                bricks[i].removeFromParent();
                bricksPhys[i].setEnabled(false);
            }
        }
        if (debugMode) {
            bas.getPhysicsSpace().enableDebug(assetManager);
        } else {
            bas.getPhysicsSpace().disableDebug();
        }

    }

    // Transform the Cylinder c such that it connects p1,p2
    private void setConnectiveTransform(float[] p1, float[] p2, Geometry c, float scaleMod) {
        // 1. direction
        Vector3f u = new Vector3f(p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]);
        float length = u.length();
        u = u.normalize();
        // 2. rotation matrix
        Vector3f v = u.cross(Vector3f.UNIT_Z);
        Vector3f w = v.cross(u);
        Matrix3f m = new Matrix3f(w.x, v.x, u.x, w.y, v.y, u.y, w.z, v.z, u.z);
        c.setLocalRotation(m);
        // 3. scaling
        c.setLocalScale(1, 1, length * PLAYER_SCALE);
        // 4. translation
        float[] center = {(p1[0] + p2[0]) / 2f, (p1[1] + p2[1]) / 2f, (p1[2] + p2[2]) / 2f};
        c.setLocalTranslation(center[0] * PLAYER_SCALE, center[1] * PLAYER_SCALE, center[2] * PLAYER_SCALE);
        // 5. scale collision shape
        RigidBodyControl rbc = c.getControl(RigidBodyControl.class);
        CollisionShape cs = rbc.getCollisionShape();
        cs.setScale(new Vector3f(1, 1, length * PLAYER_SCALE));
    }

    public void collision(PhysicsCollisionEvent event) {
        Spatial a = event.getNodeA(), b = event.getNodeB();
        if (b.getName().equals("brick") && a.getName().equals("bone")) {
            Spatial temp = a;
            a = b;
            b = temp;
        }
        if (a.getName().equals("brick") && b.getName().equals("bone")) {
            RigidBodyControl rbc = (RigidBodyControl) a.getControl(0);
            rbc.setKinematic(false);
            rbc.setMass(50);
            rbc.setGravity(new Vector3f(0, -2f, -10f));
            a.setMaterial(brokeWallMat);
            collision.play();
        }
    }

    public static void setDebugMode(boolean debugMode) {
        PhysicalBody.debugMode = debugMode;
    }

    public void setWallSpeed(float wallSpeed) {
        this.wallSpeed = wallSpeed;
    }

    public void setLightIntensity(float lightIntensity) {
        this.lightIntensity = lightIntensity;
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("shutting down");
        demoControl.dispatchEvent(new WindowEvent(demoControl, WindowEvent.WINDOW_CLOSING));
    }
}
